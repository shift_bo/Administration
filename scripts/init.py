# coding=utf-8
from flask_migrate import Migrate
from flask import current_app
from models import Employee,Role,App,Region
from service.ext.weixin import WXServer
from extends import db
from app import app
import datetime
import random
import sys
reload(sys)
sys.setdefaultencoding('utf8')
migrate = Migrate(app, db)

@app.cli.command()
def init_app_info():
    app_info = [{
        "id":1,
        "name":"快递寄送",
        "template_id":"3TmAcJCPw7hu8W48uDyv3HfYRXAQtWqZn2uuZtaG"
    },{
        "id":2,
        "name":"办公物品领用",
        "template_id":"3TmAcJCo6BjrcaAMwWWrSUPoupULXPBzBX6LKTgT"
    },{
        "id":3,
        "name":"故障报修",
        "template_id":"3TmAcJCo66vaSvXPkgPNuBkcfnm465KPgLaRQhGy"
    }]
    for i in app_info:
        app_info = App(id=i['id'],name=i['name'],template_id=i['template_id'])
        db.session.add(app_info)
    else:
        db.session.commit()
@app.cli.command()
def init_db():
    """初始化数据库"""

    # 初始化角色
    user = [{'id': 4, 'usercode': 'T-13261183393', 'username': 'bo.qin', 'name': '秦波', 'role_id': 2, 'department_id': 160,
             'email': 'bo.qin@17zuoye.com', 'avatar': 'http://wework.qpic.cn/bizmail/tqYzVytYVh8m2WdlD1flk4V0icK9dOHHVrsjqwLFiavFQibyBME6kGGww/100',
             'deleted': 0}]
    for i in user:
        empolyee = Employee(id=i['id'], usercode=i['usercode'], username=i['username'], name=i['name'], role_id=i['role_id'], department_id=i['department_id'],
                            email=i['email'], avatar=i['avatar'], deleted=i['deleted'])
        db.session.add(empolyee)
    else:
        db.session.commit()

@app.cli.command()
def sync():
    """同步部门和员工"""
    from tasks.employee import sync_employee,sync_department,update_delete_employee
    sync_employee()
    sync_department()
    update_delete_employee()

@app.cli.command()
def init_role():
    """初始化权限"""
    role = [{'id':'1','name':'MEMBER','display_name':'用户'},{'id':'2','name':'MODELADMIN','display_name':'模块管理员'},
    {'id':'3','name':'SUPERADMIN','display_name':'超级管理员'},]
    for i in role:
        db.session.add(Role(name=i['name'],display_name=i['display_name']))
    else:
        db.session.commit()

@app.cli.command()
def show_config():
    print  current_app.config.get('AGENT_ID_LIST')
