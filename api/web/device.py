#coding=utf-8

from api.handler.web import DeviceHandler
from api.base import RestApi,RestView
from flask import request
from models import db,Device,DeviceUserDetial
from service.device import DeivceService
from service.employee import EmployeeService
import time


api = RestApi('administration_device',__name__)

class Helper(object):
  @classmethod
  def device_use_detail_to_json(cls,item):
    return {
      "approvalId":item.approval_id,
      "usercode":item.usercode,
      "username":item.username,
      "usedNumber":item.used_number,
      "place":item.place,
      "departmentName":item.department_name,
      "approvalStatus":cls.jug_approval_status(int(item.approval_status)),
      "deviceName":item.device_name,
      "userTime":cls.get_approval_time(int(item.approval_status),item)
    }
  @classmethod
  def jug_approval_status(cls,approval_status):
    # SpStatus 	申请单状态：1-审批中；2-已通过；3-已驳回；4-已撤销；6-通过后撤销；7-已删除；10-已支付
    if approval_status==1:
      return "审批中"
    elif approval_status==2:
      return "已通过"
    elif approval_status==3:
      return "已驳回"
    elif approval_status==4:
      return "已撤销"
    elif approval_status==6:
      return "通过后撤销"
    elif approval_status==7:
      return "已删除"
  @classmethod
  def get_approval_time(cls,approval_status,item):
    if approval_status==1:
      return "待领取"
    elif approval_status==3:
      return "已驳回"
    elif approval_status==6:
      return "已撤回"
    else:
      return str(item.use_time)
      
      

@api.route('/device')
class DeviceApi(RestView,DeviceHandler):

  def get(self):
    device_name = request.args.get('DN')
    page = request.args.get('page')
    device_info = []
    if device_name:
      pass
    if page=='1':
      allData = []
      all_data_list = Device.query.all()
      for device in all_data_list:
        data = DeivceService.query_device_detail_info(device.id)
        allData.append(data)
    device_list = Device.query.paginate(page=int(page),per_page=20)
    device_list = device_list.items
    for device in device_list:
      data = DeivceService.query_device_detail_info(device.id)
      device_info.append(data)
    count = Device.query.count()
    if page=='1':
      return self.ok(deviceInfo=device_info,count=count,allData=allData)
    return self.ok(deviceInfo=device_info,count=count)
  def post(self):
    data = self.get_form_data()
    region_detail = request.json.get('regionDetail')
    device_name = data.get('deviceName')
    print region_detail, type(region_detail)
    result = DeivceService.add_device_by_name(device_name)
    if not result:
      return self.no(errmsg='设备重复')
    DeivceService.add_device_use_detail(region_detail,device_name)
    return self.ok()
    
@api.route('/device/change')
class DeviceChange(RestView,DeviceHandler):
  def post(self):
    if DeivceService.update_device_info(request.json):
      return self.ok()
    return self.no(errmsg='错误')

@api.route('/device/delectById')
class DeviceDelectById(RestView,DeviceHandler):
  def post(self):
    if DeivceService.delect_device_and_region_by_id(request.json):
      return self.ok()
    return self.no(errmsg='错误')



@api.route('/device/approval_detail')
class DeviceApprovalDetail(RestView):
  def get(self):
    page = request.args.get('page')
    kw = request.args.get('kw')
    if EmployeeService.current_user().role_id=="3":
      items = DeviceUserDetial.query.order_by(DeviceUserDetial.approval_id.desc()).paginate(page=int(page),per_page=20).items
      deviceUseDetialList = [Helper.device_use_detail_to_json(item) for item in items]
      count = DeviceUserDetial.query.count()
      if page=='1':
        allData = DeviceUserDetial.query.order_by(DeviceUserDetial.approval_id.desc()).all()
        allData = [Helper.device_use_detail_to_json(item) for item in allData]
        return self.ok(deviceUseDetialList=deviceUseDetialList,count=count,allData=allData)
      return self.ok(deviceUseDetialList=deviceUseDetialList,count=count)
    return '200'
