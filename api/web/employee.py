#coding=utf-8

from api.base import RestApi,RestView
from flask import request
from models import db,Employee,Role,RegionEmployee
from service.role import RoleService
from service.employee import EmployeeService
from service.region import RegionService


api = RestApi('administration_employee',__name__)

class Helper(object):
  @classmethod
  def employee_to_json(cls,item):
    return {
      "userName":item.name,
      "id":item.usercode,
      "roleId":item.role_id,
      "roleName":RoleService.query_role_name(item.role_id,item.usercode)
    }


@api.route('/employee')
class EmployeeApi(RestView):

  def get(self):
    key_word = request.args.get('key_word')
    page=request.args.get('page')
    if key_word:
      pass
    count = Employee.query.count()
    employees = Employee.query.paginate(page=int(page),per_page=15).items
    employees = [Helper.employee_to_json(item) for item in employees]
    if page=='1':
      all_data = [Helper.employee_to_json(item) for item in Employee.query.all()]
      return self.ok(employees = employees,count=count,allData=all_data)
    return self.ok(employees = employees,count=count)
  def post(self):
    pass

@api.route('/employee/currentUser')
class CurrentEmployee(RestView):
  def get(self):
    current_user = EmployeeService.current_user()
    currentUser = Helper.employee_to_json(current_user)
    return self.ok(currentUser = currentUser)

@api.route('/employee/role')
class RoleApi(RestView):
  def post(self):
    usercode = request.json.get('usercode')
    role_id = request.json.get('role_id')
    Employee.query.filter_by(usercode=usercode).update({"role_id":str(role_id)})
    db.session.commit()
    if role_id==2:
      RegionEmployee.query.filter_by(usercode=usercode).delete()
      db.session.commit()
      regionEmployee = request.json.get('regionEmployee')
      for item in regionEmployee["roleDetail"]:
        appName = item['appName']
        regionName = item['regionName']
        template_id = RegionService.query_template_id_by_app_name(appName)
        region_id = RegionService.query_region_id_by_name(regionName)
        data = RegionEmployee(usercode=usercode,template_id=template_id,region_id=region_id)
        db.session.add(data)
      db.session.commit()
    return self.ok()