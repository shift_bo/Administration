#coding=utf8

from flask import Blueprint
from api.handler.web import ExpressCompanyHandler
from api.base import RestApi,RestView
from models import db,ExpressCompany
from service.expressCompany import ExpressCompanyService

api = RestApi('administration_expressCompany',__name__)


@api.route('/expressCompany/select')
class getExpressCompany(RestView,ExpressCompanyHandler):
  """获取快递公司"""
  def post(self):
    expressCompany_info = []
    expressCompany_List = ExpressCompany.query.all()
    for expressCompany in expressCompany_List:
      data = {
        "id":expressCompany.id,
        "companyName":expressCompany.name
      }
      expressCompany_info.append(data)
    return self.ok(expressCompany_info=expressCompany_info)


@api.route('/expressCompany/insert')
class ExpressCompanyChange(RestView,ExpressCompanyHandler):
    """添加快递公司"""
  def post(self):
    if ExpressCompanyService.insert_express_company_info(request.json):
      return self.ok()
    return self.no(errmsg='错误')

    
@api.route('/expressCompany/change')
class ExpressCompanyChange(RestView,ExpressCompanyHandler):
    """修改快递公司"""
  def post(self):
    if ExpressCompanyService.update_express_company_info(request.json):
      return self.ok()
    return self.no(errmsg='错误')

@api.route('/expressCompany/delect')
class ExpressCompanyDelectById(RestView,ExpressCompanyHandler):
    """删除快递公司"""
  def post(self):
    if ExpressCompanyService.delect_express_company_by_id(request.json):
      return self.ok()
    return self.no(errmsg='错误')