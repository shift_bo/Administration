#coding=utf8

from flask import Blueprint
from app import app
from api.handler.web import RegionHandler
from api.base import RestApi,RestView
from models import db,Region,App
from service.employee import EmployeeService
from service.role import RoleService
from service.ext.weixin import WXServer

api = RestApi('administration_region',__name__)


@api.route('/region/sync')
class SyncRegion(RestView):
  def get(self):
    tempalte_id = App.query.first().template_id
    approval_model = WXServer.get_approval_model(tempalte_id)
    regions_list = approval_model["template_content"]["controls"][0]["config"]["table"]["children"][5]["config"]["selector"]["options"]
    regions = [item["value"][0]["text"] for item in regions_list]
    Region.query.delete()
    db.session.commit()
    for index,region in enumerate(regions):
      data = Region(name=region,id=int(index)+1)
      db.session.add(data)
    db.session.commit()
    return self.ok()

@api.route('/region')
class RegionApi(RestView,RegionHandler):
  """区域接口"""
  def get(self):
    current_role_name = EmployeeService.get_current_user_role_name()
    # if current_role_name == 'SUPERADMIN':
    regions = Region.query.all()
    regions = [{'id':item.id,'arearName':item.name} for item in regions]
    return self.ok(regions = regions)
    # regions = [RoleService.query_region_by_role()]
    # return self.ok(regions = regions)

  # def post(self):
  #   data = self.get_form_data()
  #   region = Region(name = data['arearName'])
  #   db.session.add(region)
  #   db.session.commit()
  #   return self.ok()

  def delete(self):
    pass

@api.route('/region/app')
class RegionAppApi(RestView):
  def get(self):
    app_items = App.query.all()
    appList = [{"id":item.id,"name":item.name} for item in app_items]
    return self.ok(appList = appList)