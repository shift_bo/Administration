# coding=utf-8
from api.base import RestApi
from api.base import RestView
from api.base import api_jsonify as jsonify
from flask import current_app
from flask import redirect
from service.ext.weixin import WXServer
from flask import request
from flask import make_response
from utils import encrypt_token
api = RestApi('api_weixin',__name__)

@api.route("/config")
class WXConfigView(RestView):
    """获取微信config"""
    def post(self):
      path = request.json.get("path") or "/"
      jssdk_config = WXservice.get_jssdk_config(path)
      return jsonify.ok(**jssdk_config)

@api.route("/oauth2/web")
class WXOauthWebLinkView(RestView):
  """微信网页授权链接"""
  
  def get(self):
    redirect_uri = current_app.config.get("WX_OAUTH2_CALLBACK_URL")
    print(111, redirect_uri)
    return redirect(WXServer.make_web_oauth_link(redirect_uri=redirect_uri))

@api.route("/oauth2/callback")
class WXOauth2CallbackView(RestView):
  """微信授权回调"""
  def get(self):
    code = request.args.get('code')
    usercode = WXServer.get_vistor_info(code)
    print usercode
    response = make_response()
    if not usercode:
      response.set_cookie("TOKEN","",max_age=0)
    else:
      token = encrypt_token(usercode)
      response.set_cookie('TOKEN',token,max_age=86400*7)
    response.headers['Location'] = "/office-goods/"
    return response,302

