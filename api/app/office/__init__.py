#coding=utf-8

from api.handler.web import RegionHandler
from api.handler.web import DeviceHandler
from api.base import RestApi,RestView
from flask import request
from models import db,Device,Region,DeviceUserDetial
from service.device import DeivceService
from service.region import RegionService
from service.ext.weixin import WXServer
from service.department import DepartmentService
from service.app import AppService
from service.employee import EmployeeService
import json

api = RestApi('office_app',__name__)


@api.route('/deviceList')
class getOfficeDeviceList(RestView,DeviceHandler):
  """获取设备列表"""
  def get(self):
    device_info = []
    device_list = Device.query.all()
    for device in device_list:
      data = {
        "id":device.id,
        "deviceName":device.name
      }
      device_info.append(data)
    return self.ok(deviceInfo=device_info)

@api.route('/regionList')
class getOfficeRegionList(RestView,RegionHandler):
  """获取楼层/区域列表"""
  def get(self):
    region_info = []
    region_list = Region.query.all()
    for region in region_list:
      data = {
        "id":region.id,
        "regionName":region.name
      }
      region_info.append(data)
    return self.ok(regionInfo=region_info)

@api.route('/wx/applyevent')
class WxApplyeventApi(RestView):
  def post(self):
    data = request.json
    count = request.json.get('count')
    department_id =request.json.get('departmentId')
    if department_id:
      pass
    else:
      data["departmentId"] = DepartmentService.query_department_id_by_name(data.get('department'))
    data_list = []
    for index in range(0,count):
      if int(index)==0:
        data_list.append({
          'user':data.get('user'),
          'departmentId':data.get('departmentId'),
          'usercode':data.get('usercode'),
          'usedNumber':data.get('usedNumber'),
          'place':data.get('place'),
          'deviceName':data.get('deviceName'),
          'department':data.get('department')
        })
      else:
	print 'usedNumber'+str(index),'number'
	print data.get('usedNumber'+str(index)),"usedNumber"
        data_list.append({
          'user':data.get('user'),
          'usercode':data.get('usercode'),
          'departmentId':data.get('departmentId'),
          'usedNumber':data.get('usedNumber'+str(index)),
          'place':data.get('place'+str(index)),
          'deviceName':data.get('deviceName'+str(index)),
          'department':data.get('department')
        })
    app_id=2
    for item in data_list:
      data = WXServer.send_approval_request(app_id,item)
      item["approval_id"] = data["sp_no"]
      DeivceService.add_device_approval_detail(item)
    return self.ok()
    
