#coding=utf8

from flask import request,Blueprint
from service.employee import EmployeeService
from service.department import DepartmentService
from service.app import AppService
from service.region import RegionService
from api.base import api_jsonify as jsonify
from models import Employee,RegionEmployee


bp = Blueprint('app_employee',__name__)

class Helper(object):
    @classmethod
    def department_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "parentId": item.parent_id,
            "type": "DEPARTMENT",
        }

    @classmethod
    def employee_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "usercode": item.usercode,
            "avatar": item.avatar,
            "departmentId": item.department_id,
            "departmentName": DepartmentService.query_department_name_by_id(item.department_id),
            "type": "EMPLOYEE",
            "roleId": item.role_id,
            "roles": [EmployeeService.get_current_user_role_name()],
        }

@bp.route('/currentuser')
def get_current_user():
  current_user = EmployeeService.current_user()
  currentUser = Helper.employee_to_json(current_user)
  return jsonify.ok(currentUser = currentUser)

@bp.route('/test')
def get_test():
    return jsonify.ok(data={"name":"qb"})   
@bp.route('/adminList')
def getAdminByAppName():
    appName = request.args.get('appName')
    template_id = RegionService.query_template_id_by_app_name(appName)
    admin_list = []
    usercode_list = []
    region_employees = RegionEmployee.query.filter_by(template_id=template_id).all()
    for item in region_employees:
        if item.usercode not in usercode_list:
            admin_list.append(Helper.employee_to_json(Employee.query.filter_by(usercode=item.usercode).first()))
        usercode_list.append(item.usercode)
    if len(admin_list) == 0:
        admin_list = [Helper.employee_to_json(item) for item in Employee.query.filter_by(role_id="3").all()]
    return jsonify.ok(adminList = admin_list)
