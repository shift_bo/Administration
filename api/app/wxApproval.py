#coding=utf-8

from api.base import RestApi,RestView
from flask import request
from service.ext.weixin import WXServer
from service.device import DeivceService
from models import DeviceUserDetial,db

from utils.xml_decrypt import ES_XML
api = RestApi('administoartion_approval',__name__)

@api.route('/cb')
class APPROVAL_CALLBACK(RestView):
  def get(self):
    data = request.args
    sReplyEchoStr = WXServer.validator_url(data)
    return sReplyEchoStr

  def post(self):
    # SpStatus 	申请单状态：1-审批中；2-已通过；3-已驳回；4-已撤销；6-通过后撤销；7-已删除；10-已支付
    data = request.args
    json_data = request.data
    xml_content = WXServer.decrypt_msg(data,json_data)
    es_xml = ES_XML(xml_content)
    if es_xml.get('SpName')=="办公用品领用":
      approval_id = es_xml.get('SpNo')
      if es_xml.get('SpStatus') == "2":
        DeivceService.office_access_approval(approval_id)
      elif es_xml.get('SpStatus') == "3":
        DeviceUserDetial.query.filter_by(approval_id=approval_id).update({
          "approval_status":"3"
        })
        db.session.commit()
      elif es_xml.get('SpStatus') == "4":
        DeviceUserDetial.query.filter_by(approval_id=approval_id).update({
          "approval_status":"4"
        })
        db.session.commit()
      elif es_xml.get('SpStatus') == "5":
        DeviceUserDetial.query.filter_by(approval_id=approval_id).update({
          "approval_status":"5"
        })
        db.session.commit()
      elif es_xml.get('SpStatus') == "6":
        DeviceUserDetial.query.filter_by(approval_id=approval_id).update({
          "approval_status":"6"
        }) 
        DeivceService.office_goback_approval(approval_id)
    elif es_xml.get('SpName')=="故障报修":            
      pass
    elif es_xml.get('SpName')=="快递寄送":
      pass
    return '200'
    
