#coding=utf8

from api.handler.web import ExpressCompanyHandler
from api.base import RestApi,RestView
from flask import request
from models import ExpressCompany,db
from service.expressCompany import ExpressCompanyService


api = RestApi('express_app',__name__)

@api.route('/expressCompanyList')
class getExpressCompany(RestView,ExpressCompanyHandler):
  """获取快递公司"""
  def post(self):
    expressCompany_info = []
    expressCompany_List = ExpressCompany.query.all()
    for expressCompany in expressCompany_List:
      data = {
        "id":expressCompany.id,
        "companyName":expressCompany.name
      }
      expressCompany_info.append(data)
    return self.ok(expressCompany_info=expressCompany_info)

@api.route('/type')
class getExpressType(RestView):
  """获取物品类型"""
  def get(self):
    pass

@api.route('/float')
class getExpressFloat(RestView):
  """获取寄件楼层"""
  def get(self):
    pass

@api.route('/commit')
class commitExpress(RestView):
  """获取寄件楼层"""
  def post(self):
    pass
