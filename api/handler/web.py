# coding=utf-8

from api.base import Parser

class RegionHandler(object):
  
  @classmethod
  def get_form_data(cls):
    parser = Parser()
    parser.add('arearName',required=True)
    return parser.get_values()

  @classmethod
  def get_args_data(cls):
    parser = Parser()
    return parser.get_values()

class DeviceHandler(object):
  
  @classmethod
  def get_form_data(cls):
    parser = Parser()
    parser.add('deviceName',required=True)
    parser.add('regionDetail',required=True,action="append")
    return parser.get_values()

  @classmethod
  def get_args_data(cls):
    parser = Parser()
    return parser.get_values()

class ExpressCompanyHandler(object):
  
  @classmethod
  def get_form_data(cls):
    parser = Parser()
    parser.add('companyName',required=True)
    return parser.get_values()

  @classmethod
  def get_args_data(cls):
    parser = Parser()
    return parser.get_values()
    