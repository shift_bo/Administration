# coding=utf-8
from celery.schedules import crontab
DEBUG = True
SITE = "http://oa-notice.test.17zuoye.net"
SECRET_KEY = "OaNoticeSystem"
SESSION_COOKIE_HTTPONLY = False
Token = "jg0ggkR7"
EncodingAESKey = "lJ8nqqi1Q8o0cJwnunSSHlh6BUAxONzIP3gpIM72aBv"
CALL_BACK_URL = "http://oa-notice.test.17zuoye.net/administration/v1/approval/cb"
#WEIXIN
WX_CORP_ID='ww496415a282ed8602'
WX_AGENT_ID_EXPRESS = '1000058'
WX_CORP_SECRET_EXPRESS = 'CQ7INCrZqHKpO5QQC8BoGUcKD5bzre3wa8C9S9ZaIyg'
WX_ACCESS_TOKEN_URL_EXPRESS= 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}'.format(
    corpid=WX_CORP_ID,corpsecret=WX_CORP_SECRET_EXPRESS
)
WX_AGENT_ID_OFFICE = '1000059'
WX_CORP_SECRET_OFFICE = 'O3ocBDoutqFdHWUTUM15AEiNHv1NAi8MUWrFaL1OiUQ'
WX_ACCESS_TOKEN_URL_OFFICE= 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}'.format(
    corpid=WX_CORP_ID,corpsecret=WX_CORP_SECRET_OFFICE
)
WX_AGENT_ID_TRUBLE = '1000060'
WX_CORP_SECRET_TRUBLE = 'FNc8JHlN1A-WkA13BFzZ9FNo4VyLoze5uBUWUmmZL9I'
WX_ACCESS_TOKEN_URL= 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}'.format(
    corpid=WX_CORP_ID,corpsecret=WX_CORP_SECRET_EXPRESS
)

WX_CORP_SECRET_APPROVAL='vwH6VTcEyenhmHMYBX4lED-AK2ob38tgwtMnnx6OAvA'
WX_ACCESS_TOKEN_URL_APPROVAL='https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}'.format(
    corpid=WX_CORP_ID,corpsecret=WX_CORP_SECRET_APPROVAL
)

WX_CORP_OAUTH2_URL = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid={CORPID}&redirect_uri={REDIRECT_URI}&response_type=code&scope=snsapi_base&state={STATE}#wechat_redirect'
WX_OAUTH2_CALLBACK_URL = SITE + '/administration/v1/weixin/oauth2/callback'

# REDIS
REDIS_URL = "redis://localhost:6379/0"

CELERY_BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = "redis://localhost:6379/1"
CELERY_IMPORTS = ["tasks.employee"]
CELERYBEAT_SCHEDULE = {
    "sync":{
        "task":"tasks.employee.sync",
        "schedule":crontab(minute="*/30")
    }
}


# consul
VOX_CONSUL_HOST = "10.7.11.196"
VOX_CONSUL_DC = "ops"

# DB
DB_CONFIG = dict(host="127.0.0.1", db="administration", username="root", password="aGVsbG9QeXRob253b3JsZA")
DB_URI = "mysql+pymysql://{}:{}@{}/{}?charset=utf8mb4".format(
    DB_CONFIG["username"], DB_CONFIG["password"], DB_CONFIG["host"], DB_CONFIG["db"]
)
SQLALCHEMY_DATABASE_URI = DB_URI
SQLALCHEMY_TRACK_MODIFICATIONS = True
