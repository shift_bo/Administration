# coding=utf-8
from service.ext.weixin import WXServer
from service.department import DepartmentService
from models import Role,Employee,db,Department
from logger import logger
from worker import celery_app
from app import app

@celery_app.task
def sync_employee():
  """同步员工信息"""
  app_info = WXServer.get_app_info()
  if not app_info:
    return 
  users = app_info['allow_userinfos']['user']
  user_ids = [i['userid'] for i in users]
  role = Role.query.filter_by(name = "MEMBER").first()
  for user_id in user_ids:
    employee = Employee.query.filter_by(usercode = user_id).first()
    if not employee:
      user_info = WXServer.get_user_info(user_id)
      if not user_info:
        continue
      update_employee(user_info,role_id = role.id)
    elif employee.deleted:
      employee.deleted = False
    db.session.commit()

@celery_app.task
def sync_department():
  """同步部门"""
  department_list = WXServer.get_department_list()
  if not department_list:
    deparmtents = Department.query.all()
    if not deparmtents:
      return ''
    else:
      DepartmentService.del_all_department_users(deparmtents)
    return ''
  department_ids = [item["id"] for item in department_list]
  for department in department_list:
    dep = Department.query.filter_by(id=department["id"]).first()
    if department["parentid"] in department_ids:
      parent_id = department["parentid"]
    else:
      parent_id = 0
    if not dep :
      dep = Department(
        id = department["id"],
        name = department["name"],
        parent_id = parent_id,
        order = department["order"]
      )
      db.session.add(dep)
    else:
        if dep.name != department["name"]:
            dep.name = department["name"]

        if dep.parent_id != parent_id:
            dep.parent_id = parent_id

        if dep.order != department["order"]:
            dep.order = department["order"]
    db.session.commit()
    #更新部门下成员
    update_department_employee(dep.id)

def update_employee(member,department_id=None,role_id=None):
    """ 更新成员
    :param member: 成员信息
    :param department_id: 部门id
    :param role_id: 角色id
    """
    if not member['email']:
      logger.error('MemberNotFoundEmail:{},{}'.format(member['userid'],member['name'].encode('utf8')))
    if not department_id:
      department_id = member['department'][-1] if member['department'] else 0
    if not role_id:
      role = Role.query.filter_by(name = "MEMBER").first()
      role_id = role.id
    username = member['email'].split('@')[0]
    avatar = member["thumb_avatar"]
    try:
      employee = Employee.query.filter_by(usercode = member["userid"]).first()
      if not employee:
        employee = Employee(
                usercode=member["userid"],
                email=member["email"],
                name=member["name"],
                username=username,
                avatar=avatar,
                role_id=role_id,
                department_id=department_id,
            )
        db.session.add(employee)
        logger.info('AddEmployee:{}'.format(member['userid']))
      else:
          employee.email = member['email']
          employee.name = member['name']
          employee.username = username
          employee.avatar = avatar
          employee.department_id = department_id
          if employee.deleted:
            employee.deleted = False
          logger.info('UpdateEmployee:{},{}'.format(employee.usercode,employee.name.encode('utf8')))
    except Exception as e :
      logger.error(str(e).encode('utf8'))
def update_department_employee(department_id):
    """ 更新部门成员
    :param department_id: 更新的部门id
    """
    logger.info('GetDepartmentMembers:{}'.format(department_id))
    members = WXServer.get_department_member_detail(department_id)
    
    if not members:
      return
    role = Role.query.filter_by(name="MEMBER").first()
    for member in members:
      update_employee(member,department_id=department_id,role_id=role.id)
    else:
      db.session.commit()
def update_delete_employee():
    """更新删除成员"""
    app_info = WXServer.get_app_info()
    if app_info:
      users = app_info["allow_userinfos"]["user"]
      usercodes = [item["userid"] for item in users]
    else :
      usercodes = []
    departments = Department.query.all()
    for department in departments:
      members = WXServer.get_department_member_detail(department.id)
      usercodes.extend([member["userid"] for member in members])
    #现有微信成员架构
    usercodes = list(set(usercodes))
    #删除用户
    employees = Employee.query.filter(
      Employee.deleted ==0,~Employee.usercode.in_(usercodes)
    )
    for employee in employees:
      employee.deleted = True
    else:
      db.session.commit()

@celery_app.task
def sync():
  sync_employee()
  sync_department()
  update_delete_employee()
