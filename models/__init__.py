# coding=utf-8

import time
from datetime import datetime

from flask_login import UserMixin

from extends import db


class Employee(db.Model):
    """ 员工表"""

    __tablename__ = "administration_employee"

    id = db.Column(db.Integer, primary_key=True)
    usercode = db.Column(db.String(50), unique=True, nullable=False)
    username = db.Column(db.String(50),nullable=True)
    name = db.Column(db.String(50), nullable=False)
    role_id = db.Column(db.String(50), nullable=False)
    department_id = db.Column(db.Integer, default=0)
    email = db.Column(db.String(75),nullable=True)
    avatar = db.Column(db.String(255))
    deleted = db.Column(db.Boolean, default=False)

class Role(db.Model):
    """权限表"""
    __tablename__ = "administration_role"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    display_name = db.Column(db.String(50), nullable=False)
    type_id = db.Column(db.Integer)
class Department(db.Model):
    """ 部门信息"""
    __tablename__ = "administration_department"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    parent_id = db.Column(db.Integer, default=0)
    order = db.Column(db.Integer, default=0)
    deleted = db.Column(db.Boolean, default=False)

class Region(db.Model):
    """区域信息"""
    __tablename__ = "administration_region"

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50),nullable=False)

class Device(db.Model):
    """设备信息"""
    __tablename__="administration_device"
    
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(50),nullable=False)

class DeviceUserDetial(db.Model):
    """设备使用详情表"""
    __tablename__="administration_device_user_detail"

    approval_id = db.Column(db.String(100),primary_key=True)
    usercode = db.Column(db.String(100), nullable=False)
    username = db.Column(db.String(100), nullable=False)
    used_number = db.Column(db.Integer,nullable=False)
    place = db.Column(db.String(50),nullable=False)
    department_name = db.Column(db.String(50),nullable=False)
    device_name = db.Column(db.String(50),nullable=False)
    approval_status = db.Column(db.Integer,nullable=False)
    use_time = db.Column(db.DateTime)

class DeviceRegion(db.Model):
    """设备信息使用标"""
    __tablename__="administration_device_region"

    id = db.Column(db.Integer,primary_key=True)
    device_id = db.Column(db.Integer,nullable=False)
    region_id = db.Column(db.Integer,nullable=False)
    used_nubmer = db.Column(db.Integer,nullable=False)
    unused_nubmer = db.Column(db.Integer,nullable=False)

class App(db.Model):
    """小程序信息"""
    __tablename__ = "administration_app"

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(100),nullable=False)
    template_id = db.Column(db.String(75),nullable=False)


class ExpressCompany(db.Model):
    """快递公司表"""
    __tablename__="administration_express_company"

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(20),nullable=False)

class RegionEmployee(db.Model):
    """员工区域表"""
    __tablename__ = "administration_region_employee"

    id = db.Column(db.Integer,primary_key=True)
    usercode = db.Column(db.String(100), nullable=False)
    template_id = db.Column(db.String(75),nullable=False)
    region_id = db.Column(db.Integer,nullable=False)