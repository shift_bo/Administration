# coding=utf-8

from flask import request
from flask import current_app
from models import Employee,Role,Department,db,RegionEmployee
from utils import decrypt_token
from service.region_employee import RegionEmployeeService

class Helper(object):
    @classmethod
    def employee_to_json(cls, item):
        return {
            "id": item.id,
            "name": item.name,
            "usercode": item.usercode,
            "avatar": item.avatar,
            "departmentId": item.department_id,
            "type": "EMPLOYEE",
            "roleId": item.role_id,
            "roleName": "",
        }

class EmployeeService(object):
    @classmethod
    def current_user(cls):
        """当前用户"""
        token = request.cookies.get('TOKEN')
        test_usercode = current_app.config.get('TEST_USERCODE')
        if test_usercode:
            return cls.get_by_usercode(test_usercode)
        try:
            usercode = decrypt_token(token)['usercode']
        except:
            return
        return cls.get_by_usercode(usercode)

    @classmethod
    def get_by_usercode(cls, usercode):
        return Employee.query.filter_by(usercode=usercode).first()

    @classmethod
    def get_current_usercode(cls):
        """获取当前usercode"""
        token = request.cookies.get('TOKEN')
        test_usercode = current_app.config.get('TEST_USERCODE')
        if test_usercode:
            return test_usercode
        try:
            usercode = decrypt_token(token)['usercode']
        except:
            return
        return usercode

    @classmethod
    def get_current_name(cls):
        """当前用户名字"""
        token = request.cookies.get('TOKEN')
        test_usercode = current_app.config.get('TEST_USERCODE')
        if test_usercode:
            return Employee.query.filter_by(usercode=test_usercode).first().name
        try:
            usercode = decrypt_token(token)['usercode']
        except:
            return
        return Employee.query.filter_by(usercode=usercode).first().name
    
    @classmethod
    def get_username_by_usercode(cls,usercode):
        """
        return 用户名字
        """
        if usercode:
            name = Employee.query.filter_by(usercode = usercode).first().name
            return name
    @classmethod
    def get_user_avatar_by_usercode(cls,usercode):
        data = Employee.query.filter_by(usercode=usercode).first()
        return data.avatar
        
    @classmethod
    def get_current_user_role_name(cls):
        data = Employee.query.filter_by(usercode=cls.get_current_usercode()).first()
        item = Role.query.filter_by(id=data.role_id).first()
        return item.name

    @classmethod
    def get_department_name_by_deparId(cls,department_id):
        data = Department.query.filter_by(id=department_id).first()
        return data.name
    
    @classmethod
    def get_role_name_by_role_id(cls,role_id):
        data = Role.query.filter_by(id=role_id).first()
        return data.display_name

    @classmethod
    def get_employeelist_by_departmentid(cls,department_id):
        """获取员工列表通过部门id"""
        items = Employee.query.filter_by(department_id=department_id).all()
        employee_list = [Helper.employee_to_json(item) for item in items]
        return employee_list

    @classmethod
    def get_current_departmentId(cls):
        usercode = cls.get_current_usercode()
        item = Employee.query.filter_by(usercode=usercode).first()
        return item.department_id

    @classmethod
    def format_message_range(cls,range_user):
        """格式化推送范围"""
        userRange = {"toparty":'',"touser":''}
        target_user = ''
        target_party = ''
        if 'all' in range_user:
            userRange["toparty"] = '@all'
            userRange["touser"] = "@all"
            return userRange
        for item in range_user:
            if type(item) is int:
                target_user+=str(item)+'|'
                userRange["toparty"] = target_user
            else:
                target_party+=str(item)+'|'
                userRange["touser"] = target_party
        return userRange

    @classmethod
    def query_approver_by_template_region_id(cls,template_id,region_id):
        try:
            approver = [RegionEmployeeService.query_admin_by_template_region_id(template_id,region_id)]
        except:
            approver = Employee.query.filter_by(role_id=3).first().usercode
        return {
            "attr":1,
            "userid":approver
        }

    @classmethod
    def del_user_by_department(cls,department_id):
        items = Employee.query.filter_by(department_id=department_id).all()
        for item in items:
            db.session.delete(item)
            db.session.commit()