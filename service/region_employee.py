#coding=utf-8

from models import RegionEmployee,db
from service.app import AppService
from service.region import RegionService
class RegionEmployeeService(object):
  @classmethod
  def to_string_role_names(cls,usercode):
    role_names = []
    items = RegionEmployee.query.filter_by(usercode=usercode).all()
    for item in items:
      app_name = AppService.query_app_name_by_template_id(item.template_id)
      region_name = RegionService.query_region_name_by_id(item.region_id)
      role_names.append(app_name+'-'+region_name)
    return role_names

  @classmethod
  def query_admin_by_template_region_id(cls,template_id,region_id):
    return RegionEmployee.query.filter_by(template_id=template_id,region_id=region_id).first().usercode