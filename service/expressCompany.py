from models import ExpressCompany


class ExpressCompanyService(object):
  @classmethod
  def query_expressCompany_id_by_name(cls,expressCompany_name):
    expressCompany = ExpressCompany.query.filter_by(name=expressCompany_name).first()
    if expressCompany:
      return expressCompany.id
    else:
      return ''

  @classmethod
  def query_expressCompany_name_by_id(cls,expressCompany_id):
    print expressCompany_id
    return ExpressCompany.query.filter_by(id=expressCompany_id).first().name

  @classmethod
  def insert_express_company_info(cls,data):
    company_name = data.get('companyName')
    expressCompany = ExpressCompany.query.filter_by(name=company_name).first()
    if expressCompany:
      return False
    else:
      print expressCompany
      expressCompany = ExpressCompany(name=company_name)
      db.session.add(expressCompany)
      db.session.commit()
      return True

  @classmethod
  def update_express_company_name_by_id(cls,id,name):
    ExpressCompany.query.filter_by(id=id).update({"name":name})

  @classmethod
  def update_express_company_info(cls,data):
    company_id = data.get('id')
    company_name = data.get('companyName')
    cls.update_express_company_name_by_id(company_id,company_name)
    return True

  @classmethod
  def delect_express_company_by_id(cls,data):
    company_id = data.get('id')
    for item in ExpressCompany.query.filter_by(id=company_id).all():
      db.session.delete(item)
      db.session.commit()
    return True