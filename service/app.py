from models import App,db

class AppService(object):
    @classmethod
    def query_app_id_by_name(cls,app_name):
      app = App.query.filter_by(name=app_name).first()
      if app:
        return app.id
      else:
        return ''

    @classmethod
    def query_app_template_id_by_id(cls,id):
      return App.query.filter_by(id=id).first().template_id

    @classmethod
    def query_app_name_by_template_id(cls,template_id):
      return App.query.filter_by(template_id=template_id).first().name