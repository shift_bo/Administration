from models import Device,db,DeviceRegion,DeviceUserDetial
from service.region import RegionService
import time
class Helper(object):
  @classmethod
  def device_detail_to_json(cls,item):
    return {
      "region":RegionService.query_region_name_by_id(item.region_id),
      "usedNumber":item.used_nubmer,
      "unUsedNumber":item.unused_nubmer,
    }
  
  @classmethod
  def device_list_to_json(cls,device_id,device_name,device_detail):
    return {
      "id":device_id,
      "deviceName":device_name,
      "regionDetail":device_detail,
      "allUsed":cls.count_all_used(device_detail),
      "allUnUsed":cls.count_all_unused(device_detail),
      "allCount":cls.count_all_used(device_detail)+cls.count_all_unused(device_detail)
    }

  @classmethod
  def count_all_used(cls,device_detail):
    all_used = 0
    for item in device_detail:
      all_used += int(item['usedNumber'])
    return all_used

  @classmethod
  def count_all_unused(cls,device_detail):
    all_unused = 0
    for item in device_detail:
      all_unused += int(item['unUsedNumber'])
    return all_unused


class DeivceService(object):

  @classmethod
  def query_device_id_by_name(cls,device_name):
    device = Device.query.filter_by(name=device_name).first()
    if device:
      return device.id
    else:
      return ''

  @classmethod
  def query_device_name_by_id(cls,device_id):
    return Device.query.filter_by(id=device_id).first().name

  @classmethod
  def query_device_detail_info(cls,device_id):
    device_detail = []
    device_name = cls.query_device_name_by_id(device_id)
    device_info_list = DeviceRegion.query.filter_by(device_id=device_id).all()
    for device_info in device_info_list:
      device_detail.append(Helper.device_detail_to_json(device_info))
    return Helper.device_list_to_json(device_id,device_name,device_detail)
      
  @classmethod
  def add_device_by_name(cls,device_name):
    device = Device.query.filter_by(name=device_name).first()
    if device:
      return False
    else:
      print device_name
      device = Device(name=device_name)
      db.session.add(device)
      db.session.commit()
      return True

  @classmethod
  def add_device_use_detail(cls,use_detail,device_name):
    device_id = cls.query_device_id_by_name(device_name)
    for item in use_detail:
      region_id = RegionService.query_region_id_by_name(item['region'])
      if not region_id:
        return False
      device_region = DeviceRegion(device_id = device_id,region_id=region_id,used_nubmer =item['usedNumber'],unused_nubmer=item['unUsedNumber'])
      db.session.add(device_region)
      db.session.commit()
    return True

  @classmethod
  def add_device_approval_detail(cls,data):
    device_use_detail = DeviceUserDetial(approval_id= data["approval_id"],usercode=data["usercode"],username=data["user"],
    used_number=data["usedNumber"],place=data["place"],department_name=data["department"],approval_status=1,device_name=data["deviceName"])
    db.session.add(device_use_detail)
    db.session.commit()

  @classmethod
  def update_device_name_by_id(cls,id,name):
    Device.query.filter_by(id=id).update({"name":name})

  @classmethod
  def update_device_info(cls,data):
    device_id = data.get('id')
    device_name = data.get('deviceName')
    device_detail = data.get('regionDetail')
    cls.update_device_name_by_id(device_id,device_name)
    for item in DeviceRegion.query.filter_by(device_id=device_id).all():
        db.session.delete(item)
        db.session.commit()
    cls.add_device_use_detail(device_detail,device_name)
    return True
    
  @classmethod
  def delect_device_and_region_by_id(cls,data):
    device_id = data.get('id')
    for item in Device.query.filter_by(id=device_id).all():
        db.session.delete(item)
        db.session.commit()
    for item in DeviceRegion.query.filter_by(device_id=device_id).all():
        db.session.delete(item)
        db.session.commit()
    return True
    
  @classmethod  
  def office_access_approval(cls,approval_id):
    DeviceUserDetial.query.filter_by(approval_id=approval_id).update(
      {"approval_status":2,"use_time":time.localtime()}
    )
    item = DeviceUserDetial.query.filter_by(approval_id=approval_id).first()
    device_name = item.device_name
    used_number = item.used_number
    region_name = item.place
    region_id = RegionService.query_region_id_by_name(region_name)
    device_id = cls.query_device_id_by_name(device_name)
    device_region_info = DeviceRegion.query.filter_by(device_id=device_id).first()
    now_used = int(device_region_info.used_nubmer)+int(used_number)
    now_unused = int(device_region_info.unused_nubmer)-int(used_number)
    DeviceRegion.query.filter_by(device_id=device_id,region_id=region_id).update({
      "used_nubmer":now_used,
      "unused_nubmer":now_unused
    })
    db.session.commit()

  @classmethod
  def office_goback_approval(cls,approval_id):
    item = DeviceUserDetial.query.filter_by(approval_id=str(approval_id)).first()
    device_name = item.device_name
    used_number = item.used_number
    region_name = item.place
    region_id = RegionService.query_region_id_by_name(region_name)
    device_id = cls.query_device_id_by_name(device_name)
    device_region_info = DeviceRegion.query.filter_by(device_id=device_id).first()
    now_used = int(device_region_info.used_nubmer)-int(used_number)
    now_unused = int(device_region_info.unused_nubmer)+int(used_number)
    DeviceRegion.query.filter_by(device_id=device_id,region_id=region_id).update({
      "used_nubmer":now_used,
      "unused_nubmer":now_unused
    })
    db.session.commit()
