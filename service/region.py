from models import Region,App


class RegionService(object):
  @classmethod
  def query_region_id_by_name(cls,region_name):
    region = Region.query.filter_by(name=region_name).first()
    if region:
      return region.id
    else:
      return ''

  @classmethod
  def query_region_name_by_id(cls,region_id):
    return Region.query.filter_by(id=region_id).first().name

  @classmethod
  def query_app_id_by_name(cls,app_name):
    return App.query.filter_by(name=app_name).first().id
  
  @classmethod
  def query_template_id_by_app_name(cls,app_name):
    return App.query.filter_by(name=app_name).first().template_id