#coding=utf-8
from models import Department,db
from service.employee import EmployeeService

class DepartmentService(object):
  @classmethod
  def del_all_department_users(cls,items):
    """删除所有部门信息以及部门下用户"""
    for item in items:
      department_id = item.id
      EmployeeService.del_user_by_department(department_id)
      cls.del_department(department_id)
      
  @classmethod
  def del_department(cls,department_id):
    target = Department.query.filter_by(id=department_id).first()
    db.session.delete(target)
    db.session.commit()
  
  @classmethod
  def query_department_name_by_id(cls,department_id):
    try:
      return Department.query.filter_by(id=department_id).first().name
    except:
      return '无'
    
  @classmethod
  def query_department_id_by_name(cls,department_name):
    return Department.query.filter_by(name=department_name).first().id