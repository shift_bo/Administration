#coding=utf-8

from models import Role,App,RegionEmployee
from service.region import RegionService
from service.region_employee import RegionEmployeeService
from service.employee import EmployeeService

class RoleService(object):
  @classmethod
  def query_role_name(cls,role_id,usercode):
    role_name = Role.query.filter_by(id=role_id).first().display_name
    if role_id=='2':
      role_names = RegionEmployeeService.to_string_role_names(usercode)
      role_names = ','.join(role_names)
      role_name = role_name+'('+role_names+')'
    return role_name

  @classmethod
  def query_region_by_role(cls):
    role_id = EmployeeService.current_user().role_id
    role_id_items = str(role_id).split('-')
    region_id = role_id_items[1]
    Region_name = RegionService.query_region_name_by_id(role_id)
    return Region_name