# coding=utf-8
from requests import request
from requests import get as req_get
from flask import current_app
from logger import logger
import base64
from utils.decorators import cached
import urllib
import json
from requests_toolbelt.multipart.encoder import MultipartEncoder
from service.app import AppService
from service.employee import EmployeeService
from service.region import RegionService
from service.ext.callback.WXBizMsgCrypt import WXBizMsgCrypt

class WXServer(object):
  @classmethod
  def request(cls,url,method="GET",params=None,data=None,headers=None):
    try:
      if headers:
        response = request(method,url,params=params,data=data,allow_redirects=False,headers=headers)
      else:
        response = request(method,url,params=params,data=data,allow_redirects=False)
      if response.status_code == 200:
        return response.json()
      else:
        return {}
    except Exception as e:
      logger.error(e.message)
      return {}
  @classmethod
  def _parse_data(cls,data,errname):
    if data.get('errcode')==0:
      return data
    elif data.get('errcode')!=0:
      logger.error(
        "{errname}({errcode}):{errmsg}".format(errname=errname,errcode=data["errcode"],errmsg=data["errmsg"])
      )
    return {}
      
  @classmethod
  @cached(expires=7000)
  def get_access_token(cls,approval=False):
    """获取access_token"""
    if approval:
      url = current_app.config.get("WX_ACCESS_TOKEN_URL_APPROVAL")
    else:
      url = current_app.config.get("WX_ACCESS_TOKEN_URL_OFFICE")
    data = cls.request(url)
    data = cls._parse_data(data,"AccessTokenError")
    return data.get('access_token')

  @classmethod
  def get_app_info(cls):
    """获取应用"""
    agent_id = current_app.config.get("WX_AGENT_ID_OFFICE")
    url = "https://qyapi.weixin.qq.com/cgi-bin/agent/get?access_token={ACCESS_TOKEN}&agentid={AGENTID}"
    url = url.format(ACCESS_TOKEN=cls.get_access_token(),AGENTID=agent_id)
    data = cls.request(url)
    data = cls._parse_data(data,"AppInfo") 
    print('token', cls.get_access_token())
    return data

  @classmethod
  def get_user_info(cls,user_id):
    """获取用户信息"""
    url = 'https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={ACCESS_TOKEN}&userid={USERID}'
    url = url.format(ACCESS_TOKEN = cls.get_access_token(),USERID = user_id)
    data = cls.request(url)
    data = cls._parse_data(data,"UserInfo")
    return data

  @classmethod
  def get_department_list(cls):
    access_token = cls.get_access_token()
    url = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={ACCESS_TOKEN}"
    url = url.format(ACCESS_TOKEN=access_token)
    data = cls.request(url)
    data = cls._parse_data(data,'GetDepartmentList')
    return data.get("department") or []

  @classmethod
  def get_department_member_detail(cls,department_id,fatch_child="0"):
    """获取部门成员详情"""
    access_token = cls.get_access_token()
    url = "https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token={ACCESS_TOKEN}&department_id={DEPARTMENT_ID}&fetch_child={FETCH_CHILD}"
    url = url.format(ACCESS_TOKEN=access_token,DEPARTMENT_ID=department_id,FETCH_CHILD=fatch_child)
    data = cls.request(url)
    data = cls._parse_data(data,'GetDepartmentMemberDetail')
    return data.get('userlist') or []

  @classmethod
  def make_web_oauth_link(cls,redirect_uri,state=""):
    """构造网页授权链接"""
    appid = current_app.config.get('WX_CORP_ID')
    redirect_uri = urllib.quote(redirect_uri)
    corp_oauth2_url = current_app.config.get("WX_CORP_OAUTH2_URL")
    url = corp_oauth2_url.format(CORPID=appid,REDIRECT_URI=redirect_uri,STATE=state)
    return url

  @classmethod
  def get_vistor_info(cls,code):
    """获取访问者身份"""
    url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={ACCESS_TOKEN}&code={CODE}"
    url = url.format(ACCESS_TOKEN=cls.get_access_token(),CODE = code)
    data = cls.request(url)
    data = cls._parse_data(data,'GetVistorInfo')
    return data.get("UserId","")

  @classmethod
  def send_message(cls,message):
    """发送推送"""
    url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={ACCESS_TOKEN}".format(
      ACCESS_TOKEN=cls.get_access_token()
    )
    data = cls.request(url=url,method="POST",data=message)
    data = cls._parse_data(data,'SendMessage')
    return data
  
  @classmethod
  def create_message(cls,notice_info,userRange={"touser":"@all","toparty":"@all"},img_url=None):
    """构造推送data"""
    descript = NoticeService.get_notice_descript(notice_info["content"])
    title = notice_info["title"]
    touser = userRange["touser"]
    toparty = userRange["toparty"]
    data =     {
       "touser" : touser,
       "toparty" : toparty,
       "totag" : "TagID1 | TagID2",
       "msgtype" : "news",
       "agentid" : current_app.config.get('WX_AGENT_ID'),
       "news" : {
           "articles" : [
               {
                   "title" : title,
                   "description" : descript,
                   "url" : "http://oa-notice.test.17zuoye.net/?id={}".format(notice_info["id"]),
                   "picurl" : img_url
               }
            ]
       },
       "enable_id_trans": 0,
       "enable_duplicate_check": 0
    }
    resp_data = cls.send_message(json.dumps(data))
    invaliduser = resp_data.pop('invaliduser',None)
    invalidtype = resp_data.pop('invalidtype',None)
    if  invaliduser:
      logger.error('[Notice][Entry: {} ] [Invaliduser] {}'.format(notice_info.id,invaliduser))
    if invalidtype:
      logger.error('[Notice][Entry: {} ] [Invalidtype] {}'.format(notice_info.id,invalidtype))
    return data
  
  @classmethod
  def post_img_wx(cls,path=None,index=None):
    url = "https://qyapi.weixin.qq.com/cgi-bin/media/uploadimg?access_token={ACCESS_TOKEN}".format(ACCESS_TOKEN=cls.get_access_token())
    if path:
       data = MultipartEncoder(
        fields={
        'field0':("defaultImg",open(path,'rb'),'image/png')
          } 
        )
    else:
       data = MultipartEncoder(
        fields={
        'field0':("defaultImg",open(ImageServer.get_default_img_path(index),'rb'),'image/png')
          } 
        )
    headers={
      "Content-Type":data.content_type
    }
    response = cls.request(url=url,method='POST',data=data,headers=headers)
    data = cls._parse_data(response,'POST_IMG')
    return data.pop('url',None)

  @classmethod
  @cached(expires=7000)
  def get_jsapi_ticket(cls):
    """获取sdk企业ticket"""
    url = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token={ACCESS_TOKEN}".format(ACCESS_TOKEN=cls.get_access_token())  
    response = cls.request(url=url)
    data = cls._parse_data(response,'GET_JSAPI_TICKET')
    return data.get('ticket')
    
  @classmethod
  @cached(expires=7000)
  def get_jsapi_ticket_agent_config(cls):
    """获取sdk应用ticket"""
    url = "https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token={ACCESS_TOKEN}&type=agent_config".format(ACCESS_TOKEN=cls.get_access_token())  
    response = cls.request(url=url)
    data = cls._parse_data(response,'GET_JSAPI_TICKET')
    return data.get('ticket')

  @classmethod
  def get_cache_material_img(cls,media_id):
    """获取临时素材"""
    url = "https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token={ACCESS_TOKEN}&media_id={MEDIA_ID}".format(ACCESS_TOKEN=cls.get_access_token(),
    MEDIA_ID=media_id)
    response = req_get(url=url)
    with open('./pic.png','wb') as f:
      f.write(response.content)
      f.close
    img = open('./pic.png', 'rb')
    img = img.read()
    img_stream = base64.b64encode(img)
    return img_stream
    
  
  @classmethod
  def send_approval_request(cls,app_id,approval_message):
    """发送审批申请"""
    url = "https://qyapi.weixin.qq.com/cgi-bin/oa/applyevent?access_token={ACCESS_TOKEN}".format(
      ACCESS_TOKEN=cls.get_access_token(True)
    )
    data =  cls.create_approval(app_id,approval_message)
    data = cls.request(url=url,method="POST",data=json.dumps(data, ensure_ascii=False, encoding='utf8').encode('utf8'))
    data = cls._parse_data(data,'send_approval_request')
    return data

  @classmethod
  def get_approval_model(cls,template_id):
    url = "https://qyapi.weixin.qq.com/cgi-bin/oa/gettemplatedetail?access_token={ACCESS_TOKEN}".format(
      ACCESS_TOKEN=cls.get_access_token()
    )
    body_data = {
      "template_id":template_id
    }
    data = cls.request(url=url,method="POST",data=json.dumps(body_data))
    data = cls._parse_data(data,'get_approval_model')
    return data

  @classmethod
  def create_approval(cls,app_id,approval_message):
    template_id = AppService.query_app_template_id_by_id(app_id)
    model = cls.get_approval_model(template_id)
    approver = EmployeeService.query_approver_by_template_region_id(template_id,RegionService.query_region_id_by_name(approval_message['place']))
    return {
      "creator_userid":approval_message['usercode'],
      "template_id":template_id,
      "use_template_approver":0,
      "approver":[approver],
      "notifyer":[],
      "notify_type":1,
      "apply_data":{"contents":cls.create_apply_data_model(model['template_content']['controls'],approval_message)},
      "summary_list":[{
        "summary_info":[{"text":"审批人员姓名:{name}".format(name=approval_message["user"]),"lang": "zh_CN"}],
      },{
        "summary_info":[{"text":"审批人员帐号:"+approval_message["usercode"],"lang": "zh_CN"}],
      }]
    }

  @classmethod
  def create_apply_data_model(cls,approval_model,approval_message):
    controls = approval_model
    apply_data = []
    item_data = {
      "value":{}
    }
    for control in controls:
      for key in control:
        if key == 'property':
          item_data["control"] = control[key]["control"]
          item_data["id"] = control[key]["id"]
          if control[key]["control"] == "Contact" and control["config"]["contact"]["mode"]=="user":
            item_data["value"]["members"] = [{
              "userid":approval_message["usercode"],
              "name":approval_message["user"]
            }]
          elif control[key]["control"] == "Table":
            item_data["value"]["children"]=[
              {
                "list":cls.create_apply_data_model(control["config"]["table"]["children"],approval_message)
              }
            ]
          elif control[key]["control"] == "Contact" and control["config"]["contact"]["mode"]=="department":
            item_data["value"]["departments"] = [{
              "openapi_id":approval_message["departmentId"],
              "name":"一起教育科技" if approval_message["department"]=="无" else approval_message["department"]
            }]
          elif control[key]["control"] == "Text" and control[key]["title"][0]["text"]=="办公用品名称":
            item_data["value"]["text"] = approval_message["deviceName"]
          elif control[key]["control"] == "Number" and control[key]["title"][0]["text"]=="办公用品数量":
            item_data["value"]["new_number"] = approval_message["usedNumber"]
          elif control[key]["control"] == "Selector" and control[key]["title"][0]["text"]=="领取楼层":
            item_data["value"]["selector"] = {
              "type":"single",
              "options":[
                {
                  "key":cls.get_options_key_by_value(control["config"]["selector"]["options"],approval_message["place"]),
                  "value":[
                    {
                    "text":approval_message["place"],
                    "lang":"zh_CN"
                  }
                  ]
                }
              ]
            }
          apply_data.append(item_data)
          item_data = {"value":{}}
    return apply_data

  @classmethod
  def get_options_key_by_value(cls,model_options,select_value):
    for item in model_options:
      print item
      if item["value"][0]["text"] == select_value:
        print item["key"],"key"
        return item["key"]
          

  @classmethod
  def validator_url(cls,data):
    sToken = "DFVrVCWC8tFW1Q8nQaUSJa"
    sEncodingAESKey = "0zn0xd317eEu9tuyDVXbfchhsoMAnbTUX6q6bsgMyjQ"
    sReceiveId = "ww496415a282ed8602"
    sMsgSignature = data.get('msg_signature')
    sTimeStamp = data.get('timestamp')
    sNonce = data.get('nonce')
    sEchoStr = data.get('echostr')
    #sReplyEchoStr =  解密之后的echostr，当return返回0时有效
    wxcpt = WXBizMsgCrypt(sToken=sToken,sEncodingAESKey=sEncodingAESKey,sReceiveId=sReceiveId)
    ret,sReplyEchoStr = wxcpt.VerifyURL(sMsgSignature = sMsgSignature,sTimeStamp=sTimeStamp,sNonce=sNonce,sEchoStr=sEchoStr)
    if ret==0:
      return sReplyEchoStr

  @classmethod
  def decrypt_msg(cls,args,json_data):
    sToken = "DFVrVCWC8tFW1Q8nQaUSJa"
    sEncodingAESKey = "0zn0xd317eEu9tuyDVXbfchhsoMAnbTUX6q6bsgMyjQ"
    sReceiveId = "ww496415a282ed8602"
    sMsgSignature = args.get('msg_signature')
    sTimeStamp = args.get('timestamp')
    sNonce = args.get('nonce')
    sPostData = json_data
    wxcpt = WXBizMsgCrypt(sToken=sToken,sEncodingAESKey=sEncodingAESKey,sReceiveId=sReceiveId)
    ret,xml_content = wxcpt.DecryptMsg(sPostData=sPostData,sMsgSignature=sMsgSignature,sTimeStamp=sTimeStamp,sNonce=sNonce)
    if ret==0:
      return xml_content