# coding=utf8
from binascii import a2b_hex
from binascii import b2a_hex

from Crypto.Cipher import AES


class Cryptor(object):

    SECRET_KEY = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

    @classmethod
    def ensure_text(cls, text):
        length, count = 16, len(text)
        add = length - (count % length)
        text += '\0' * add
        return text

    @classmethod
    def encrypt(cls, text):
        text = cls.ensure_text(text)
        crypt = AES.new(cls.SECRET_KEY[:16], AES.MODE_CBC, cls.SECRET_KEY[:16])
        plain_text = crypt.encrypt(text)
        return b2a_hex(plain_text)

    @classmethod
    def decrypt(cls, text):
        crypt = AES.new(cls.SECRET_KEY[:16], AES.MODE_CBC, cls.SECRET_KEY[:16])
        plain_text = crypt.decrypt(a2b_hex(text))
        plain_text = plain_text.rstrip('\0')
        return plain_text
