from xml.dom.minidom import parseString

class ES_XML(object):
  def __init__(self,xml_string):
    self.collections = parseString(xml_string).documentElement
  
  def get(self,tag_name):
    return self.collections.getElementsByTagName(tag_name)[0].childNodes[0].data