# coding=utf-8
from functools import wraps
from flask import request
from flask import jsonify
from utils import decrypt_token
from flask import current_app
from extends import redis_store
from service.employee import EmployeeService

def token_required(func):
    """TOKEN认证"""
    @wraps(func)
    def deco(*args, **kw):
        token = request.cookies.get('TOKEN')
        test_usercode = current_app.config.get("TEST_USERCODE")
        print test_usercode
        if test_usercode:
            return func(*args, **kw)

        if not token:
            return jsonify({{"status": 401, "errmsg": 'NotFoundToken', "data": {}}})
        try:
            decrypt_token(token)
        except Exception:
            return jsonify({"status": 401, "errmsg": "InvalidToken", "data": {}})

        return func(*args, **kw)
    return deco

def cache_class_property(key=None):
    """缓存类属性"""
    def cache_property(func):
        _cache_key = key if key else func.__name__
        @warps(func)
        def deco(*args,**kw):
            cls = args[0]
            if hasattr(cls,_cache_key):
                return getattr(cls,_cache_key)
            else :
                result = func(*args,**kw)
                setattr(cls,_cache_key,result)
                return result
        return deco
    return cache_property

def cached(key=None,expires=600):
    """缓存函数
    :param store:存储对象
    """
    def _cache_function(func):
        _cache_key = key if key else func.__name__
        @wraps(func)
        def deco(*args,**kw):
            cls = args[0]
            cache_key = cls.__name__ + "." +_cache_key
            if redis_store.exists(cache_key):
                return redis_store.get(cache_key)
            token = func(*args,**kw)
            if token:
                redis_store.set(cache_key,token,expires)
            return token
        return deco
    return _cache_function

def allow_range(*args):
    """权限控制"""
    def allow_func(func):
        @wraps(func)
        def _allow_func():
            if EmployeeService.get_current_user_role_name() not in args:
                return jsonify({"status":401,"errmsg":"InvalidRole",data:{}})
            return func()
        return _allow_func
    return allow_func 